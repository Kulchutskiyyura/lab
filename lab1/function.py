from constant import*
from Token import Token
from check_function import cheack_if_number
import copy 
import parserr
from html_function import find_tag
from html_function import find_tag_with_html
from html_function import get_tag_type
from html_function import get_page
from html_function import get_tag_atribute
from html_function import set_tag_atribute
from html_function import del_tag_atribute
from html_function import find_tag_inner_html
from PySide2.QtCore import QObject
from PySide2 import QtCore, QtWidgets
from exception_class import Eror
from PySide2.QtWidgets import QInputDialog
import sys
def run_function(function):

    if function[0]._value not in funct_dict and function[0]._value not in user_funct_dict:
        raise Eror("\n function "+function[0]._value+" is not define",eror_way=function[0]._value)
        return None
    #-------------------------------------------------------------------------------------------------------------
    if function[0]._value=="print":
        return_value=Print(*function[1:])
        return return_value
    elif function[0]._value=="type":
        if len(function[1:])!=funct_dict["type"]:
            print("len argument in type = ",len(function[1:]))
            raise Eror("function need "+str(funct_dict["type"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None
        print("type function")
        return_value=Type(function[1])
        return return_value
    elif function[0]._value=="int":
        if len(function[1:])!=funct_dict["int"]:
            raise Eror("function need "+str(funct_dict["int"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None
        try:
            return_value=Int(function[1])
            eror=False
        except Eror as e:
            eror =Eror(e.eror_text)
        except  Exception:
             eror= Eror("invalid syntax")
        if type(eror)==Eror:
                 raise eror
                 return
        return return_value
    elif function[0]._value=="float":
        if len(function[1:])!=funct_dict["float"]:
             raise Eror("function need "+str(funct_dict["float"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
             return None
        try:
            return_value=Float(function[1])
            eror=False
        except Eror as e:
            eror =Eror(e.eror_text)
        except  Exception:
             eror= Eror("invalid syntax")
        if type(eror)==Eror:
                 raise eror
                 return
        return return_value
    elif function[0]._value=="string":
        if len(function[1:])!=funct_dict["string"]:
             raise Eror("function need "+str(funct_dict["string"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
             return None
        return_value=String(function[1])
        return return_value
    elif function[0]._value=="input":
        if len(function[1:])!=funct_dict["input"]:
             raise Eror("function need "+str(funct_dict["input"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
             return None
        return_value=Input(function[1])
        return return_value
    #----------------------------------------------------------------------------------------------------------------------------
    elif function[0]._value=="get_page":
        if len(function[1:])!=funct_dict["get_page"]:
             raise Eror("function need "+str(funct_dict["get_page"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
             return None
        return_value=get_page(function[1]._value,True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    elif function[0]._value=="find_tag":
        if len(function[1:])!=funct_dict["find_tag"]:
             raise Eror("function need "+str(funct_dict["find_tag"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
             return None
        return_value=find_tag(function[1]._value,function[2]._value,int(function[3]._value),user_use=True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    elif function[0]._value=="get_tag_type":
        if len(function[1:])!=funct_dict["get_tag_type"]:
             raise Eror("function need "+str(funct_dict["get_tag_type"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
             return None
        return_value=get_tag_type(function[1]._value,user_use=True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    elif function[0]._value=="find_tag_with_html":
        if len(function[1:])!=funct_dict["find_tag_with_html"]:
            raise Eror("function need "+str(funct_dict["find_tag_with_html"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None
        return_value=find_tag_with_html(function[1]._value,function[2]._value,int(function[3]._value),user_use=True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    elif function[0]._value=="find_tag_inner_html":
        if len(function[1:])!=funct_dict["find_tag_inner_html"]:
            raise Eror("function need "+str(funct_dict["find_tag_inner_html"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None
        return_value=find_tag_inner_html(function[1]._value,function[2]._value,int(function[3]._value),user_use=True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    #-------------------------------------------------------------------------------------------------------------
    elif function[0]._value=="get_tag_atribute":
        if len(function[1:])!=funct_dict["get_tag_atribute"]:
            raise Eror("function need "+str(funct_dict["get_tag_atribute"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None
        return_value=get_tag_atribute(function[1]._value,function[2]._value,user_use=True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    elif function[0]._value=="set_tag_atribute":
        if len(function[1:])!=funct_dict["set_tag_atribute"]:
            raise Eror("function need "+str(funct_dict["set_tag_atribute"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None
        return_value=set_tag_atribute(function[1]._value,function[2]._value,function[3]._value,user_use=True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    elif function[0]._value=="del_tag_atribute":
        if len(function[1:])!=funct_dict["del_tag_atribute"]:
            raise Eror("function need "+str(funct_dict["del_tag_atribute"])+"argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None
        return_value=del_tag_atribute(function[1]._value,function[2]._value,user_use=True)
        if return_value==None:
            return Token("",STRING)
        return return_value
    #---------------------------------------------------------------------------------------------------------------------
    else:

        #raise Eror("function need "+str(len(local_var_dict[user_funct_dict[function[0]._value][len(function[1:])][0]))+" but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
           #        return None
       
        if user_funct_dict[function[0]._value].get(len(function[1:])) !=None:
            print("\n\n\nuser function\n\n\n",len(user_funct_dict[function[0]._value][len(function[1:])][0]),len(function[1:]))
            local_var_dict_copy = copy.deepcopy(local_var_dict) 

            if len(user_funct_dict[function[0]._value][len(function[1:])][0]) != len(function[1:]):
                print("\n\n\problem with argument\n\n\n")
               
            local_var_dict.clear()
            print("before argument input function",function[0]._value," local_var_dict= ",local_var_dict)
            print("local_var_dict_copy= ",local_var_dict_copy)
            for i in range(len(function[1:])):
                local_var_dict[user_funct_dict[function[0]._value][len(function[1:])][0][i]._value]=function[1+i]
            print("before parser in user function",function[0]._value," local_var_dict= ",local_var_dict)
            print("global var_dict= ",var_dict)
            try:
                return_value=parserr.parser(user_funct_dict[function[0]._value][len(function[1:])][1],local=True)
                eror=False
            except Eror as e:
                eror= Eror(e.eror_text,eror_way=function[0]._value+"\\"+e.eror_way)
            if type(eror)==Eror:
                 print("eror in user fun",eror)
                 raise eror
                 return
            print("after parser in user function ",function[0]._value,"local_var_dict= ",local_var_dict)
            print("global var_dict= ",var_dict)
            print(" local_var_dict_copy",local_var_dict_copy)
            local_var_dict.clear()
            local_var_dict.update(local_var_dict_copy)
            print("local var dict after del= ",local_var_dict)
            if return_value==None:
                return_value=Token("0",NUMBER)
            else:
                return_value=return_value.data
            return return_value
        else:
            number_of_argument=""
            for key in user_funct_dict[function[0]._value]:
                number_of_argument+=str(key)+"or"
                
            number_of_argument=number_of_argument[:-2]
            raise Eror("function "+function[0]._value+" need "+number_of_argument+" argument but "+str(len(function[1:]))+" given",eror_way=function[0]._value)
            return None





def Print(*args):
    print("\n\n\n\n\n")
    file = open('print.txt', 'a')
    value=""
    for i in args:
        print(i._value,end="")
        file.writelines(str(i._value))
        value+=str(i._value)
    print("\n\n\n\n\n")
    file.write("\n\n\n")
    file.close()
    if len(value)<20:
        Interfacee[0].print_area.insertPlainText(value+"\n")
    else:
        Interfacee[0].print_area.insertPlainText(value+"\n\n")
    return Token(str(len(args)),NUMBER)

def Type(argument):
    return Token(argument._typee,STRING)

def Int(argument):
    if cheack_if_number(argument._value):
        print("\n\ncheack_if_number!=-1\n\n")
        return Token(str(int(float(argument._value))),NUMBER)
    else:
        print("\n\ncheack_if_number==-1\n\n")
        raise Eror("\ncant convert "+str(argument._value)+" to int",eror_way=" int")
        return None
def Float (argument):
    if cheack_if_number(argument._value):
        return Token(str(float(argument._value)),NUMBER)
    else:

        raise Eror("\ncant convert "+str(argument._value)+" to float",eror_way=" float")
        return None
def String (argument):
    return Token(argument._value,STRING)

def Input(argument):

    input  = QInputDialog().getText(Interfacee[0], "input", argument._value[:-1], QtWidgets.QLineEdit.Normal)
    #global  constant.INTERFACE
    print("\n\n\n input ==",input,"\n\n\n")
    return_value=input[0]
    return Token(return_value,STRING)
    
