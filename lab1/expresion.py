from constant import *
from Token import Token
from linker import linker
from separator import separator
from check_function import *
from find_function import*
from operation import Binary_operators
from operation import Unary_operation
from function import run_function
import copy
from exception_class import Eror
class Interpretator():
    break_counter=0
    if_is_true=True
    def __init__(self,token_list,string="tree is "):
        Interpretator.break_counter+=0
        Interpretator.if_is_true
        self.token_list=token_list
        self.eror=False
        self.tree_value=" "
        self.string=string


    def create_tree(self,string,list_of_token,list_with_pattern,must_be_variable=False):
        #string.isalnum() or self.cheack_if_number(string) or string in list_with_pattern or self.is_string(string)
        #print(must_be_variable)
        #зробити пріорітети логічним операторам
        #rfind_index_of_token(string,EQB,SIGN)!=-1 or rfind_index_of_token(string,NEQ,SIGN)!=-1 or rfind_index_of_token(string,AND,SIGN)!=-1 or
        if   rfind_index_of_token(string,OR,SIGN)!=-1:
            print("or string",string)
            index=rfind_index_of_token(string,OR,SIGN)
            list_of_token.append(string[index])

            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif   rfind_index_of_token(string,AND,SIGN)!=-1:
            print("and string",string)
            index=rfind_index_of_token(string,AND,SIGN)
            list_of_token.append(string[index])
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif rfind_index_of_token(string,EQB,SIGN)!=-1 or rfind_index_of_token(string,NEQ,SIGN)!=-1:
            index=0
                       ## print("addd")
            eq=rfind_index_of_token(string,EQB,SIGN)
            neq=rfind_index_of_token(string,NEQ,SIGN)
            if eq>neq:
                index=eq
            else :
                index=neq
            list_of_token.append(string[index])
           ## print(index)
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif rfind_index_of_token(string,MORE,SIGN)!=-1 or rfind_index_of_token(string,LESS,SIGN)!=-1:
            index=0
                       ## print("addd")
            more=rfind_index_of_token(string,MORE,SIGN)
            less=rfind_index_of_token(string,LESS,SIGN)
            if more>less:
                index=more
            else :
                index=less
            list_of_token.append(string[index])
           ## print(index)
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif rfind_index_of_token(string,ADD,SIGN)!=-1 or rfind_index_of_token(string,SUB,SIGN)!=-1:
            index=0
                       ## print("addd")
            add=rfind_index_of_token(string,ADD,SIGN)
            sub=rfind_index_of_token(string,SUB,SIGN)
            if add>sub:
                index=add
            else :
                index=sub
            list_of_token.append(string[index])
           ## print(index)
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif rfind_index_of_token(string,MUL,SIGN)!=-1 or rfind_index_of_token(string,DIV,SIGN)!=-1:
            ##print("mulll")
            index=0
            mul=rfind_index_of_token(string,MUL,SIGN)
            div=rfind_index_of_token(string,DIV,SIGN)
            if mul>div:
                index=mul
            else :
                index=div

            list_of_token.append(string[index])
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif   rfind_index_of_token(string,POWER,SIGN)!=-1:
            print("power string",string)
            index=rfind_index_of_token(string,POWER,SIGN)
            list_of_token.append(string[index])
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif   rfind_index_of_token(string,NOT,SIGN)!=-1:
            index=rfind_index_of_token(string,NOT,SIGN)
            list_of_token.append(string[index])
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif   rfind_index_of_token(string,SUB_UNAR,SIGN)!=-1:
            index=rfind_index_of_token(string,SUB_UNAR,SIGN)
            list_of_token.append(string[index])
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        elif   rfind_index_of_token(string,FOR_SIGN,SIGN)!=-1:
            index=rfind_index_of_token(string,FOR_SIGN,SIGN)
            list_of_token.append(string[index])
            try:
                self.create_tree(string[0:index],list_of_token,list_with_pattern,True)
                self.create_tree(string[index+1:len(string)],list_of_token,list_with_pattern,True)
                eror=False
            except Eror as e:
                 eror= Eror(e.eror_text)
            except Exception:
                 eror= Eror("invalid syntax")
            if type(eror)==Eror:
                raise eror
                return
        else:
            if len(string)==1 and string[0]._typee!=SIGN:
                if string[0]._typee==NUMBER or (string[0]._value in list_with_pattern and string[0]._typee==PATERN) :
                    print("token number is",string[0])
                    list_of_token.append(string[0])

                    return
                elif string[0]._typee==COMMENT:
                     return
                elif string[0]._typee==STRING:
                #string=string[1:len(string)-1]
                    print("is string", string)

                    list_of_token.append(string[0])

                elif string[0]._typee==VAR:
                    eror=False
                    try:
                    #print("qqqqqqqqqqqqqqqqqqqqqqqq")
                        if must_be_variable:
                            list_of_token.append(string[0])
                            print("must be var!!!")
                        else:
                            print("must not be var!!!",must_be_variable)
                            token=local_var_dict[string[0]._value]
                            list_of_token.append(token)


                    except KeyError:


                            print("second try")
                            print(var_dict.get(string[0]._value))
                            if var_dict.get(string[0]._value) is None:
                                 #print("indefinite varible "+string)
                                 eror= Eror("\n indefinite varible "+string[0]._value)

                            if not eror:
                                token=var_dict[string[0]._value]
                                print("second try end")
                                list_of_token.append(token)
                    if type(eror)==Eror:
                        raise eror


                else:
                    return

    def create_final_expresion(self,string,number,list_with_pattern):
        #var x=print(print(4,5,8));
        if len(string)==0:
            raise Eror("\n invalid sybtax")
        if string[0]._typee==PATERN and string[0]._value==RUN_FUNCTION:
                string=string[1:]
                function_argument=find_function_argument(string)
                if function_argument==None:
                    raise Eror("ivalid syntax ")
                tree.append("function call : "+string[0]._value)
                tree.append("begin")
                expresion=""
                for i in function_argument:
                    for j in i :
                        expresion+=j._value
                    expresion+="  "
                tree.append("function argument(expresion)   is "+ expresion)
                print("function argument ",function_argument)


                list_with_argument_value=[]
                for i in function_argument:
                    argument=self.create_final_expresion(i,0,[])[0]
                    try:

                        value=self.calculate(argument)
                        eror=False
                    except Eror as e:
                        eror= Eror("problem with function argument "+"in function "+string[0]._value+"\n"+e.eror_text,eror_way=string[0]._value+"\\"+e.eror_way)
                    if type(eror)==Eror:
                        raise eror
                        return
                    list_with_argument_value.append(value)
                print("function argument value  ",list_with_argument_value)
                expresion=""
                for i in list_with_argument_value:
                    expresion+=i._value+" "
                tree.append("function argument(value)   is "+ expresion)
                final_function=create_function_using_functname_and_functargumen(string[0],list_with_argument_value)
                print("final function ",final_function)
                try:
                    return_value=run_function(final_function)

                    eror=False
                except Eror as e:

                        eror= Eror(e.eror_text,eror_way=e.eror_way)
                if type(eror)==Eror:
                        raise eror
                        return
                tree.append("function return is "+return_value._value)
                tree.append("end")
                #self.tree_value+=return_value._value
                return [[return_value],[return_value]]
        index=find_index_of_token(string,"(",BREAK)
        if index==-1:
                token_list=[]

                self.create_tree(string,token_list,list_with_pattern)
                token_copy=copy.deepcopy(token_list)
                print("\n\ntoken list if index ==-1",token_list)

                if not list_with_pattern:

                    try:
                        value=self.calculate(token_list)
                    except Eror as e:
                        raise Eror(e.eror_text,eror_way=e.eror_way)
                        return
                    token_list=[value]

                print("token_list",token_list)
                return [token_list,token_copy]
        else:
            counter=1
            index_2=index
            for i in range(index+1,len(string)):
                if string[i]._value=="(" and string[i]._typee==BREAK:
                    counter+=1
                if string[i]._value==")" and string[i]._typee==BREAK:
                    counter-=1
                if counter==0:
                    index_2=i
                    break
            else:
                print(string)
                raise Eror("problem with ) with index " +str(index+1) )
                return
            str_patern="patern_eror"
            if index-1>=0 and string[index-1]._typee==FUNCTION:
                 string_new=[Token(RUN_FUNCTION,PATERN)]+[string[index-1]]+string[index+1:index_2]
                 str_patern="__"+str(number)
                 string=string[:index-1]+[Token(str_patern,PATERN)]+string[index_2+1:]
            else:
                string_new=string[index+1:index_2]
                str_patern="__"+str(number)
                string=string[:index]+[Token(str_patern,PATERN)]+string[index_2+1:]
            print("list with token after patern",string)
            print("string new",string_new)
            list_with_pattern.append(str_patern)
            number+=1
            print("string -",string)
            try:
                both_list1=self.create_final_expresion(string,number,list_with_pattern)
                both_list2=self.create_final_expresion(string_new,0,[])
                list1=both_list1[0]
                list2=both_list2[0]
                tree_list1=both_list1[1]
                tree_list2=both_list2[1]
                eror=False
            except Eror as e:
                   eror= Eror(e.eror_text,eror_way=e.eror_way)
            if type(eror)==Eror:
                        raise eror
                        return
            print("prev",list1,list2)
            eror=False
            try:
                index=list1.index(Token(str_patern,PATERN))
                index_for_tree_list=tree_list1.index(Token(str_patern,PATERN))
            except ValueError:
                eror=Eror("ivalid syntax ")
            if type(eror)==Eror:
                raise eror
            del list1[index]
            del tree_list1[index_for_tree_list]
            list1=list1[:index]+list2+list1[index:]
            tree_list1=tree_list1[:index_for_tree_list]+ tree_list2 +tree_list1[index_for_tree_list:]
            print("return",list1)
            return [list1,tree_list1]





    def calculate(self,list_of_token):
        if self.eror:
            return
        value=0
        print("token_list calcul",list_of_token)

        list_reverse=list_of_token[::-1]
        print("list rev  before   ",list_reverse)
        while len(list_reverse)>1:

            sign=list(filter(lambda x: x._typee==SIGN,list_reverse))[0]

            index=list_reverse.index(sign)
            if  list_reverse[index]._value in[NOT,SUB_UNAR] :
                 print(list_reverse[index-1])

                 print(list_reverse[index])

                 try:
                    calculation=Unary_operation(list_reverse[index],list_reverse[index-1])
                    value=calculation.eval()
                    eror=False
                 except Eror as e:
                    eror= Eror(e.eror_text)

                 except Exception:
                     eror= Eror("invalid syntax")
                 if type(eror)==Eror:
                        raise eror
                        return
                 list_reverse[index-1]=value
                 del list_reverse[index:index+1]
                 print("unar oper end",list_reverse)
            else:

                index=list_reverse.index(sign)
                print(list_reverse[index-1])
                print(list_reverse[index-2])
                print(list_reverse[index])
            #поміняти назву змінної
                try:
                    calculation=Binary_operators(list_reverse[index],list_reverse[index-1],list_reverse[index-2])
                    value=calculation.eval()
                except Eror as e:
                     raise Eror(e.eror_text)
                     return
                except Exception:
                     raise Eror("\ninvalid syntax")
                     return
                list_reverse[index-2]=value
                del list_reverse[index-1:index+1]
                print(len(list_reverse),index-2)


        print("list rev  ",list_reverse)
        if len(list_reverse)==0:
            raise Eror("\ninvalid syntax")
            return
        return list_reverse[0]


    def var_defenition(self):



        #print("rext var_def",self.text)
        #index=self.text.index(EQ)


        #self.create_token_list(self.text[index+1:])
        #self.calculate()
        #final_list_with_token=[]
        #list_with_token=linker(self.text)
        print("var_dif , list with token ",self.token_list)
        try:
            final_list_with_token=self.create_final_expresion(self.token_list,0,[])
            eror=False
        except Eror as e:
               eror= Eror(e.eror_text,eror_way=e.eror_way)
        if type(eror)==Eror:
                raise eror
                return
        final_tree=""

        if final_list_with_token==None:
            raise Eror("\n invalid syntax")
        print("final list rev  ",final_list_with_token[0])
        self.value=self.calculate(final_list_with_token[0])
        for i in final_list_with_token[1]:
            if i._typee!=PATERN:
                final_tree+=i._value+" "
        final_tree=self.string+ final_tree
        tree.append(final_tree)
        print(self.value)
        return self.value
        #var_dict[self.text[0:index]]=self.value


def create_function_using_functname_and_functargumen(funct_name,argument_list):
    list_of_token_function=[funct_name]

    return list_of_token_function+argument_list
