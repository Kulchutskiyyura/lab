from Statmen import find_index_of_token
from Statmen import rfind_index_of_token
from Statmen import Interpretator
from Statmen import Asigment
from Statmen import If_statment
from Statmen import While_statment
from Statmen import Function_statment
from Statmen import Function_definition_statment
from Statmen import Return_statment
from Statmen import For_statment
from constant import *
from Token import Token
from parser_return import Parser_return
from exception_class import Eror
from separator import separator
from linker import linker
import copy
def parser(list_of_token,local=False):

    print('new parser')
    print("list of token in parser",list_of_token)
    token_index=0
    is_loop=False
    can_go_in_elif=False
    can_use_el_if=False
    while(1):
        #print("\n\n\n\nlocal var dict in parser ",local_var_dict)
        if token_index>=len(list_of_token):
            raise Eror("\n invalid syntax")
            return
#--------------------------------------------------------------------------------------------------------------------------------------------------
        if list_of_token[token_index]._typee==VAR:
            if list_of_token[token_index+1]._value!=EQ or list_of_token[token_index+1]._typee!=SIGN:
                print("dont have = after varible","variable =" ,list_of_token[token_index]._value)
                print("next token= ",list_of_token[token_index-5:token_index+3])
                raise Eror("dont have = after varible "+list_of_token[token_index]._value)
                return
            else:
                print("asigment")
                eror=False
                try:
                    asigment=Asigment(list_of_token[token_index:])
                    next=asigment.next()
                except Eror as e:
                    eror=e

                if type(eror)==Eror:
                    raise Eror("problem with asigment to variable "+list_of_token[token_index]._value+eror.eror_text,eror_way=eror.eror_way)
                    return
                if next==None :
                    break
                else:
                    print("next=",next)
                    print("token index",token_index)
                    print("list_of_token",list_of_token)
                    token_index+=next
#----------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==DEFINITION:
             if list_of_token[token_index+2]._value!=EQ or list_of_token[token_index+2]._typee!=SIGN:
                 raise Eror("dont have = after varible "+list_of_token[token_index]._value)
                 return None
             if list_of_token[token_index+1]._typee!=VAR:
                 raise Eror(list_of_token[token_index+1]._value+" is not variable")
                 return None

             if local:
                 print("var def")
                 if local_var_dict.get(list_of_token[token_index+1]._value)!=None:
                     raise Eror("redefinition of variable "+list_of_token[token_index+1]._value)
                     return None
                 local_var_dict[list_of_token[token_index+1]._value]=Token(list_of_token[token_index+1]._value,VAR)
             else:
                print("var def")
                if var_dict.get(list_of_token[token_index+1]._value)!=None:
                    raise Eror("redefinition of variable "+list_of_token[token_index+1]._value)
                    return None
                var_dict[list_of_token[token_index+1]._value]=Token(list_of_token[token_index+1]._value,VAR)
             eror=False
             try:
                    asigment=Asigment(list_of_token[token_index:],True)
                    next=asigment.next()
             except Eror as e:
                    eror=e

             if type(eror)==Eror:
                    raise Eror("problem with variable  defenition "+list_of_token[token_index+1]._value+"\n"+eror.eror_text,eror_way=eror.eror_way)
                    return

             if next==None :
                   break
             else:
                   token_index+=next
        elif list_of_token[token_index]._typee==FUN_DEFINITION:
            if  list_of_token[token_index+1]._typee!=FUNCTION:
                 print("dont have fun after fun def (parser)", list_of_token[token_index+1])
                 raise Eror("dont have function after fun definition ("+list_of_token[token_index+1]._value+")")
                 return None
            print("function def  parser")
            print("curent token list ")

            eror=False
            try:
                     fun_definition=Function_definition_statment(list_of_token[token_index+1:])
                     next=fun_definition.next()
            except Eror as e:
                    eror=e

            if type(eror)==Eror:
                    raise Eror("problem with fun defenition "+list_of_token[token_index]._value+eror.eror_text,eror_way=eror.eror_way)
                    return
            print("next fun def",next)
            if next==None :
                   break
            else:
                   token_index+=next
                   print("token_index fun def ",token_index)
#-----------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==IF:
            if (list_of_token[token_index+1]._value!="(" or list_of_token[token_index+1]._typee!=BREAK) and list_of_token[token_index]._value!=ELSE:
                 raise Eror("dont have ( after if "+list_of_token[token_index+1]._value)
                 return None
            else:
                 print("\n\n\nif parser\n\n\n")
                 if list_of_token[token_index]._value==IF:
                     can_use_el_if=True
                 if list_of_token[token_index]._value in [ELSE,ELIF] and not can_use_el_if:
                      raise Eror("cant use  "+list_of_token[token_index]._value+" before if statment")
                 if ((list_of_token[token_index]._value==ELIF or list_of_token[token_index]._value==ELSE) and not can_go_in_elif):
                     if_statment= If_statment(list_of_token[token_index:],True)
                 else:
                    if_statment= If_statment(list_of_token[token_index:])

                 eror=False
                 try:

                     next=if_statment.next()
                 except Eror as e:
                    eror=e

                 if type(eror)==Eror:
                    raise Eror("problem with  "+list_of_token[token_index]._value+eror.eror_text,eror_way=eror.eror_way)
                    return

                 if next==None :
                    break
                 else:
                    token_index+=next
                 can_go_in_elif=if_statment.go_to_elif
#-----------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==LOOP and list_of_token[token_index]._value==WHILE :
             if (list_of_token[token_index+1]._value!="(" or list_of_token[token_index+1]._typee!=BREAK) :
                 print("dont have ( after while (parser)")
                 raise Eror("dont have ( after while "+list_of_token[token_index+1]._value)
                 return None
             else:
                 print("\n\n while \n\n\n")

                 eror=False
                 try:

                     while_statment= While_statment(list_of_token[token_index:])
                     next=while_statment.next()
                 except Eror as e:
                    eror=e

                 if type(eror)==Eror:
                    raise Eror("problem with while \n"+list_of_token[token_index]._value+eror.eror_text,eror_way=eror.eror_way)
                    return
                 tree.append("end")
                 if next==None :
                    break
                 elif next==0:
                    print("\n\n next =0 \n\n\n")
                    local_var_dict_copy = copy.deepcopy(local_var_dict)
                    parser_return=parser(list_of_token[token_index+while_statment.index_begin_while+1:while_statment.index_end_while+token_index],True)
                    for i in local_var_dict:
                         if local_var_dict_copy.get(i)!=None:
                              local_var_dict_copy[i]=local_var_dict[i]
                    local_var_dict.clear()
                    local_var_dict.update(local_var_dict_copy)
                    if parser_return==None:
                        token_index+=next
                    elif type(parser_return)==Parser_return:
                        if parser_return.return_type==RETURN:
                            return parser_return
                        elif parser_return.return_type==LOOP_BREAK:
                            token_index+=while_statment.index_end_while
                    else:
                        print("\n\neror with parser return(LOOP parser)\n\n\n")
                 else:
                     token_index+=next
#------------------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==LOOP and list_of_token[token_index]._value==FOR :
             if (list_of_token[token_index+1]._value!="(" or list_of_token[token_index+1]._typee!=BREAK) :
                 raise Eror("\ndont have ( after for "+list_of_token[token_index+1]._value)
                 return None
             else:
                 print("\n\n for  \n\n\n")

                 for_statment=For_statment(list_of_token[token_index:])
                 if local_var_dict.get(for_statment.variable._value)==None and var_dict.get(for_statment.variable._value)==None:
                     raise Eror("\nno such variable in for ")
                     print("no such variable in for(parser)")
                 for i in for_statment:
                    if i==None:
                        print("\n\n\n eror in for statmen iteration\n\n\n")
                        raise Eror("\n invalid syntax ")
                        return
                    if local_var_dict.get(for_statment.variable._value)!=None:
                        local_var_dict[for_statment.variable._value]=i
                    else:
                        var_dict[for_statment.variable._value]=i
                    parser_return=parser(list_of_token[token_index+for_statment.index_begin_for_body+1:for_statment.index_end_for_body+token_index],True)


                    if type(parser_return)==Parser_return:
                        if parser_return.return_type==RETURN:
                            tree.append("end")
                            return parser_return
                        elif parser_return.return_type==LOOP_BREAK:
                            tree.append("end")
                            break
                 tree.append("end")
             token_index+=for_statment.index_end_for_body-1
             print("token index after for =",token_index,"len token list",len(list_of_token))
             if token_index>len(list_of_token):
                 return
#--------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==FUNCTION:
            if (list_of_token[token_index+1]._value!="(" or list_of_token[token_index+1]._typee!=BREAK) :
                 raise Eror("\n dont have ) after function "+list_of_token[token_index]._value)
                 return None
            print("function parser")
            eror=False
            try:

                     function_statment=Function_statment(list_of_token[token_index:])
                     next= function_statment.next()
            except Eror as e:
                    eror=Eror(e.eror_text,eror_way=list_of_token[token_index]._value+"\\"+e.eror_way)

            if type(eror)==Eror:
                    raise Eror("problem with function "+list_of_token[token_index]._value+eror.eror_text,eror_way=eror.eror_way)
                    return

            if next==None :
                    print("function break")
                    break
            else:
                    print("function")
                    print("next=",next)
                    print("token index",token_index)
                    print("list_of_token",list_of_token)
                    token_index+=next
#----------------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==RETURN:
             if local==False:
                 print("\n\n return in global space \n\n\n")
             return_statment=Return_statment(list_of_token[token_index:])
             if return_statment.eror:
                  raise Eror("\nproblem with return \n"+return_statment.eror_text,return_statment.eror_way)
                  return

             print("return parser")
             if return_statment.eror==False:
                 return Parser_return (return_statment.value,RETURN)
             else:
                 raise Eror(return_statment.eror_text,return_statment.eror_way)
#-------------------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==LOOP_BREAK:
            print("break loop  parser")
            return Parser_return (None,LOOP_BREAK)
#-------------------------------------------------------------------------------------------------------------------------------------------------------
        elif list_of_token[token_index]._typee==BREAK:
            print("break parser")
            if token_index+1<len(list_of_token):
                token_index+=1
            else:
                break

    print("\n\n\n\n\n parser finish his work, local= ",local,"\n\n\n")


