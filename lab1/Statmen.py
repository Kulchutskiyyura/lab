from abc import ABC
from abc import abstractclassmethod
from constant import*
from expresion import Token
from expresion import Interpretator
from expresion import find_index_of_token
from expresion import rfind_index_of_token
from expresion import find_end_of_brackets_index
from expresion import find_function_argument
from check_function import cheack_if_arguments_is_the_same
from for_iterator import For_iterator
from exception_class import Eror
#from math import a

class Statment(ABC):
    @abstractclassmethod
    def __init__(self,list_of_token):
        pass
    @abstractclassmethod
    def next(self):
        pass



class Asigment(Statment):
    def __init__(self,list_of_token,is_defenition=False):

        self.index=find_index_of_token(list_of_token,END,BREAK)
        indentation=2+is_defenition
        self.eror=False
        self.eror_text=""
        self.eror_way=""

        self.id=list_of_token[int(is_defenition)]._value
        if is_defenition:
            tree.append("Definition to "+ self.id)
        else:
            tree.append("Asigment to "+ self.id)
        tree.append("begin")
        expresion=""
        for i in list_of_token[indentation:self.index]:
             expresion+=i._value
        tree.append("expression is "+ expresion)
        try:

            interpretator=Interpretator(list_of_token[indentation:self.index])
            self.value=interpretator.var_defenition()
            tree.append("value is "+ self.value._value)
        except Eror as e:
              self.eror=True
              self.eror_text=e.eror_text
              self.eror_way=e.eror_way
        #print("self.value(asigment)", self.value)
        self.list_of_token=list_of_token

        if local_var_dict.get(self.id)!=None:
            self.local_var=True
        elif var_dict.get(self.id)!=None:
            self.local_var=False
        else:
            self.eror_text=" \n variable "+self.id+" is not define"+"\n"+self.eror_text
            self.eror=True


    def next(self):
        if  self.eror:
            print("eror is  ",self.eror_text)
           # print(self.value)
            #print(self.id)
            raise Eror(self.eror_text,eror_way=self.eror_way)
            print("after exception")
            return
        if self.value==None:
            print("asigment value is none")
            raise Eror("invalid syntax")
            return None

        if id==None:
            print("asigment id is none")
            raise Eror("invalid syntax")
            return None
        print("next_as")
        if self.eror==False:
            if self.local_var:
                local_var_dict[self.id]=self.value
            else:
                var_dict[self.id]=self.value
        else:
            pass
        tree.append("end")
        if self.index+1<len( self.list_of_token):
             return self.index+1
        return None

class If_statment(Statment):
    def __init__(self, list_of_token,can_not_use=False):
        self.id=list_of_token[0]._value
        self.go_to_elif=False
        self.index_begin_if=find_index_of_token(list_of_token,"{",BREAK)
        self.index_end_if=find_end_of_brackets_index(list_of_token,"{","}")
        self.eror=False
        self.eror_text=""
        self.eror_way=""
        self.exp=0.0000000001
        self.list_of_token=list_of_token
        if can_not_use==False:


            tree.append(self.id+" statment")
            expresion=""
            for i in list_of_token[1:self.index_begin_if]:
                expresion+=i._value
            tree.append("condition expresion  is "+ expresion)
            tree.append("begin")
            if (self.id!=ELSE):
                try:
                    interpretator=Interpretator(list_of_token[1:self.index_begin_if])
                    self.value=interpretator.var_defenition()
                    tree.append("condition value is "+ self.value._value)
                except Eror as e:
                    self.eror=True
                    self.eror_text=e.eror_text
                    self.eror_way=e.eror_way
                tree.append("if condition is true")
                tree.append("next token is " +str(list_of_token[self.index_begin_if+1] if self.index_begin_if+1<len(list_of_token) else "None"))
                tree.append("if condition is false")
                tree.append("next token is " +str(list_of_token[self.index_end_if+1] if self.index_end_if+1<len(list_of_token) else "None"))
            else:
                if self.index_begin_if!=1:
                    raise Eror("\n invalid syntax")
                self.value=Token("1",NUMBER)


        else:
            tree.append("begin")
            self.value=Token("0",PATERN)

    def next(self):
         if   self.eror:
            raise Eror(self.eror_text,eror_way=self.eror_way)
            return
         if self.value==None:
            print("if value is none")
            raise Eror("\ninvalid syntax")
            return None
         if   self.index_end_if==None:
            raise Eror("\n dont have } ")
            return
         tree.append("end")
         if self.value._typee==NUMBER:
             if abs(float(self.value._value))<self.exp:
                 # x * 10 if x < 10 else x / 10
                 if self.index_end_if+1<len(self.list_of_token) and self.list_of_token[self.index_end_if+1]._typee==IF and (self.list_of_token[self.index_end_if+1]._value==ELIF or self.list_of_token[self.index_end_if+1]._value==ELSE):
                     self.go_to_elif=True
                 return self.index_end_if+1 if self.index_end_if+1<len(self.list_of_token) else None
             else:
                  return self.index_begin_if+1 if self.index_begin_if+1<len(self.list_of_token) else None
         elif self.value._typee==STRING:
             if self.value._value=="":
                  if self.index_end_if+1<len(self.list_of_token) and self.list_of_token[self.index_end_if+1]._typee==IF and (self.list_of_token[self.index_end_if+1]._value==ELIF or self.list_of_token[self.index_end_if+1]._value==ELSE):
                      self.go_to_elif=True
                  return self.index_end_if+1 if self.index_end_if+1<len(self.list_of_token) else None
             else:
                 return self.index_begin_if+1 if self.index_begin_if+1<len(self.list_of_token) else None
         elif self.value._typee==PATERN:
              return self.index_end_if+1 if self.index_end_if+1<len(self.list_of_token) else None

class While_statment(Statment):
    def __init__(self, list_of_token):
        self.id=list_of_token[0]._value
        self.index_begin_while=find_index_of_token(list_of_token,"{",BREAK)
        self.eror=False
        self.eror_text=""
        self.eror_way=""
        self.index_end_while=find_end_of_brackets_index(list_of_token,"{","}")
        tree.append(self.id+" statment")
        tree.append("begin")
        expresion=""
        for i in list_of_token[1:self.index_begin_while]:
             expresion+=i._value
        tree.append("condition expresion  is "+ expresion)
        try:
            interpretator=Interpretator(list_of_token[1:self.index_begin_while])
            self.value=interpretator.var_defenition()
            tree.append("condition value is "+ self.value._value)
        except Eror as e:
              self.eror=True
              self.eror_text=e.eror_text
              self.eror_way=e.eror_way
        tree.append("if condition is true")
        tree.append("next token is " +str(list_of_token[self.index_begin_while+1] if self.index_begin_while+1<len(list_of_token) else "None"))
        tree.append("if condition is false")
        tree.append("next token is " +str(list_of_token[self.index_end_while+1] if self.index_end_while+1<len(list_of_token) else "None"))
        self.list_of_token=list_of_token
        self.exp=0.0000000001
    def next(self):
        if   self.eror:
            raise Eror(self.eror_text,eror_way=self.eror_way)
            return
        if self.value==None:
            print("while value is none")
            raise Eror("\ninvalid syntax")
            return None
        if  self.index_end_while==None:
            raise Eror("\n dont have } ")
            return
        if self.value._typee==NUMBER:
             if abs(float(self.value._value))<self.exp:
                 # x * 10 if x < 10 else x / 10

                 return self.index_end_while+1 if self.index_end_while+1<len(self.list_of_token) else None
             else:
                  return 0
        elif self.value._typee==STRING:
             if self.value._value=="":

                  return self.index_end_while+1 if self.index_end_while+1<len(self.list_of_token) else None
             else:
                 return 0
class Function_statment(Statment):
    def __init__(self, list_of_token):
           self.index=find_index_of_token(list_of_token,END,BREAK)
           self.id=list_of_token[0]._value
           self.eror=False
           self.eror_text=""
           self.eror_way=""

           try:
               interpretator=Interpretator(list_of_token[0:self.index])
               self.value=interpretator.var_defenition()
           except Eror as e:
              self.eror=True
              self.eror_text=e.eror_text
              self.eror_way=e.eror_way

           #print("self.value(function)", self.value)
           self.list_of_token=list_of_token
    def next(self):
        if   self.eror:
            raise Eror(self.eror_text,eror_way=self.eror_way)
            return
        if self.value==None:
            print("function value is none")
            raise Eror("invalid syntax")
            return None

        if id==None:
            print("function id is none")
            return None
        print("function next")
        if self.index+1<len( self.list_of_token):
             return self.index+1
        return None
class Function_definition_statment(Statment):
    def __init__(self, list_of_token):
        #додати перевірку на неоднаковість параметрів

        self.id=list_of_token[0]._value
        self.eror=False
        self.eror_text=""
        self.eror_way=""
        index_of_first_bracket=find_index_of_token(list_of_token,"(",BREAK)
        index_of_second_bracket=find_index_of_token(list_of_token,")",BREAK)
        self.number_of_del_token=2
        del list_of_token[index_of_first_bracket]
        del list_of_token[index_of_second_bracket-1]
        self.index_begin_fun=find_index_of_token(list_of_token,"{",BREAK)
        self.index_end_fun=find_end_of_brackets_index(list_of_token,"{","}")
        self.function_argument=find_function_argument(list_of_token[: self.index_begin_fun])
        if (user_funct_dict.get(self.id)!=None and user_funct_dict[self.id].get(len(self.function_argument))!=None )or funct_dict.get(self.id)!=None:
            self.eror_text="redifinition of function"
            self.eror_way=self.id
            self.eror=True
        if cheack_if_arguments_is_the_same(self.function_argument):
            print("function have the same argument(function defenition)")
            self.eror_text="function have the same argument"
            self.eror_way=self.id
            self.eror=True
        tree.append("function definition  "+self.id)
        self.list_of_token=list_of_token
    def next(self):
        if self.eror:
            raise Eror(self.eror_text,eror_way=self.eror_way)
            return
        if user_funct_dict.get(self.id)==None:
            user_funct_dict[self.id]={}
            #*self.function_argument
        if self.index_end_fun==None:
            raise Eror("\n dont have } ")
            return
        user_funct_dict[self.id][len(self.function_argument)]=[[item[0] for item in self.function_argument],self.list_of_token[self.index_begin_fun+1: self.index_end_fun]]
        print(user_funct_dict)
        if self.index_end_fun+1<len( self.list_of_token):
             return self.index_end_fun+self.number_of_del_token
        return None

class Return_statment(Statment):
    def __init__(self, list_of_token):
        self.index=find_index_of_token(list_of_token,END,BREAK)
        self.eror=False
        self.eror_text=""
        self.eror_way=""
        self.id=list_of_token[0]._value
        try:
             interpretator=Interpretator(list_of_token[1:self.index])
             self.value=interpretator.var_defenition()
        except Eror as e:
              self.eror=True
              self.eror_text=e.eror_text
              self.eror_way=e.eror_way
        #print("self.value(return)", self.value)
        self.list_of_token=list_of_token

    def next(self):

        if   self.eror:
            raise Eror(self.eror_text,eror_way=self.eror_way)
            return
        if self.value==None:
            print("while value is none")
            raise Eror("invalid syntax")
            return None
        print("returnstatment_next_function")
        if self.eror==True:
            pass
        return None

class For_statment():
    def __init__(self, list_of_token):
        self.id=list_of_token[0]._value
        self.index_begin_for_body=find_index_of_token(list_of_token,"{",BREAK)
        self.index_end_for_body=find_end_of_brackets_index(list_of_token,"{","}")
        tree.append(self.id+" statment")
        tree.append("begin")
        expresion=""
        for i in list_of_token[1:self.index_begin_for_body]:
             expresion+=i._value
        tree.append("for expresion  is "+ expresion)
        interpretator=Interpretator(list_of_token[1:self.index_begin_for_body])
        self.value=interpretator.var_defenition()
        self.sources=self.value[1]
        #перейменувати
        self.variable=self.value[0]
        self.list_of_token=list_of_token
        #self.exp=0.0000000001
    def __iter__(self):
        return For_iterator(local_var_dict[self.sources._value]._value if local_var_dict.get(self.sources._value)!=None else var_dict[self.sources._value]._value)


           
        





    




    

   


    
