class Eror(Exception):
    def __init__(self,eror_text,eror_position=0,eror_way=""):
        self.eror_text=eror_text
        self.eror_position=eror_position
        self.eror_way=eror_way
    def __str__(self):
        return repr(self.eror_way+"\n"+self.eror_text)
